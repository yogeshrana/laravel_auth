<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

@extends('layouts.main')
    @section('content')

<div class="pro_main" style="position:absolute;right:400px">
    @error('name')
        <span class="" style="color:red" >
            <strong>{{ $message }}</strong>
        </span>
    @enderror
        <br>
    @error('email')
        <span class="" style="color:red" >
            <strong>{{ $message }}</strong>
        </span>
    @enderror
        <div class="cont_ainer">
            <div class="title_top_profile">
            </div>
            <div class="profile_main">
                <div class="prfile_left">

                </div>
                <div class="profile_right">
                    <div class="tab-content" id="justifiedTabContent">
                    <h5 style="text-align:center">Edit Profile</h5>
                        <div aria-labelledby="home-tab" class="tab-pane active" id="Profile" role="tabpanel">
                            <div class="profile_content">
                                <div class="profile title_bar view_profile">
                                </div>
                                <form action="{{route('edit_profile')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="profile_comman_view">
                                         <div class="user_profile">

                                                <div class="profile_camera">
                                                    <img height="70px" width="70px" src="{{ $user->profile_photo_url }}" id="blah">
                                                    <input class="upload_file" id="imgInp" onchange="readfile(event)" accept="image/*"
                                                        name="profile_image" type="file">
                                                </div>

                                            </div>
                                        <div class="profile_view_feild">
                                            <div class="feild_profile">
                                                <div class="fild_inner">
                                                    <label class="label_field" for="email">User Name</label>
                                                    <input class="form_control" placeholder="User Name"
                                                        name="name"
                                                        value="{{ old('name', $user->name ?? '') }}"
                                                        type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="feild_outer">
                                            <div class="form_feild_inner_left">
                                                <label class="label_field" for="email">Email</label>
                                                <input class="form_control" placeholder="Email" name="email"
                                                    value="{{ old('email', $user->email ?? '') }}" type="email">
                                            </div>
                                        </div>
                                        <div class="feild_outer">
                                            <div class="form_feild_inner_left">
                                                <label class="label_field" for="email">Phone Number</label>
                                                <input class="form_control" placeholder="Enter Phone Number" name="phone"
                                                    value="{{ old('phone', $user->phone ?? '') }}" type="text">
                                            </div>
                                        </div>
                                        <div class="feild_outer">
                                            <div class="form_feild_inner_left">
                                                <label class="label_field" for="email">Address</label>
                                                <input class="form_control" placeholder="Enter Address" name="address"
                                                    value="{{ old('address', $user->address ?? '') }}" type="text">
                                            </div>
                                        </div>

                                        <div class="button_outer">
                                            <button type="submit" class="btn btn_primary">Submit</button>
                                    </div>
                                </form>
                                <div class="flash-message">
                                @if(session('success'))
                                <p class="" style="color:green">{{ session('success') }} </p>
                                @endif
                            </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
<script>
var readfile = function(event) {

    $('#blah').attr('src', URL.createObjectURL(event.target.files[0]));

};
</script>
@include('include.footer')
