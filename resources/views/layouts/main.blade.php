<!DOCTYPE html>
<html lang="en">

@include('include.head')

<body>
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
   
    @include('include.sidebar')
  
    @include('include.header')

    @yield('content')

