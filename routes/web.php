<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::user()){
        return view('index');
    }
    return view('auth-signin');
});

Route::get('/tbl_bootstrap', [App\Http\Controllers\TableController::class, 'users'])->name('tbl')->middleware('guestuser');

Route::get('/bc_button', function () {
    return view('bc_button');
});

Route::get('/bc_badges', function () {
    return view('bc_badges');
});

Route::get('/form_elements', function () {
    return view('form_elements');
});

Route::get('/chart-morris', function () {
    return view('chart-morris');
});

Route::get('/map-google', function () {
    return view('map-google');
});

Route::get('/auth-signup', function () {
    if(Auth::user()){
        return view('index');
    }
    return view('auth-signup');
});

Route::get('/auth-signin', function () {
    if(Auth::user()){
        return view('index');
    }
    return view('auth-signin');
});

Route::get('/sample-page', function () {
    return view('sample-page');
});

Route::get('index', function () {
    if(Auth::user()){
        return view('index');
    }
    return view('auth-signin');
});

Route::get('/bc_breadcrumb-pagination', function () {
    return view('bc_breadcrumb-pagination');
});

Route::get('/bc_collapse', function () {
    return view('bc_collapse');
});

Route::get('/bc_tabs', function () {
    return view('bc_tabs');
});

Route::get('/bc_typography', function () {
    return view('bc_typography');
});

Route::get('/icon-feather', function () {
    return view('icon-feather');
});

Route::get('/auth-reset-password', function () {
    return view('auth-reset-password');
});

Auth::routes();

Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('profile', [App\Http\Controllers\UserController::class, 'profile'])->name('profile');
Route::post('profile', [App\Http\Controllers\UserController::class, 'edit_profile'])->name('edit_profile');
Route::post('auth-signup', [App\Http\Controllers\UserController::class, 'user_register'])->name('user_register');
Route::post('/', [App\Http\Controllers\UserController::class, 'user_login'])->name('user_login');
Route::get('logout', [App\Http\Controllers\UserController::class, 'logout'])->name('logout');
Route::get('stripe-payment', [App\Http\Controllers\StripeController::class, 'handleGet']);
Route::post('stripe-payment', [App\Http\Controllers\StripeController::class, 'handlePost'])->name('stripe.payment');
