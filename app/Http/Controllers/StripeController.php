<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Stripe;
use Session;
use App\Helpers\StripeHelper;
use Auth;

class StripeController extends Controller
{
    /**
     * payment view
     */
    public function handleGet()
    {
        return view('payment');
    }
  
    /**
     * handling payment with POST
     */
    public function handlePost(Request $request)
    {
        $cardToken = $request->stripeToken;
        $amount = 100 * 100;
        $user = Auth::User();
        //  return $user->email;
       
       $customer = StripeHelper::addCustomer($user);
    //    $card = StripeHelper::createCard($user,$request);
       $charge = StripeHelper::createDirectCharge($user, $cardToken, $amount, $desc=null);
        // Stripe\Stripe::setApiKey(config('common.stripe_secret_key'));
        // Stripe\Charge::create ([
        //         "amount" => 100 * 120,
        //         "currency" => "inr",
        //         "source" => $request->stripeToken,
        //         "description" => "Making test payment." 
        // ]);
  
        Session::flash('success', 'Payment has been successfully processed.');
          
        return back();
    }
}