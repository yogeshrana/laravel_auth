<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class TableController extends Controller
{
    public function users(Request $request){
        // if(Auth::user()->isAdmin()){
        $users = User::where('id','!=',NULL);
		if (isset($request->name)) {
			$users =$users->where('name','LIKE','%'.$request->name.'%');
		}
		$users =$users->paginate(5);
		return view('tbl_bootstrap')->with('users', $users);
    // }
    // return view('index');
    }
}
