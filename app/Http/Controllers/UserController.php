<?php

namespace App\Http\Controllers;
use Hash;
use Auth;
use Session;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    function user_register(Request $req)
    {
        $user = new User;
        $user->name = $req->uname;
        $user->email = $req->email;
        $user->password =Hash::make($req->password);
        $user->save();
        return view('auth-signin');
    }

    public function user_login(Request $request)
    {	
    	 if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

   			 return view('index');
		        }

        return "error";
    
    }

    public function profile()
    {	
        $user = Auth::user();
        return view('profile',compact('user'));
    }
   
    public function edit_profile(Request $request)
    {	
     
        $rules = ([
			'name' => 'required|min:3',
            'email' => 'required|email',
		]);

        $message = ([
			'name.required' => 'User Name is Required',
			'name.min' => 'User Name has minimun 3 character',
			'email.required' => 'Email  is Required',
			'email.email' => ' Please Enter Email Formatted',
		]);
        $request->validate($rules,$message);
        $data = $request->except(['_token']);
        // $user->update($data);
        if ($request->hasFile('profile_image')){
            // $filename = $request->profile_image->getClientOriginalName();
            // $request->profile_image->storeAs('assets/images',$filename,'public');
            $data['profile_image'] = $this->fileUpload($request->profile_image, 'assets/images')['name'] ?? null;
        }
        $user=User::where('id',Auth::user()->id)->update($data);
        $user = User::find(Auth::user()->id);
        return redirect()->route('profile',compact('user'))->with('success','Your Profile is Updated');
    }

    public function logout()
    {	
        Auth::logout();
        return view('auth-signin');
    }
}
