<?php

namespace App\Helpers;
class StripeHelper
{

    /**
     *  create a stripe customer for a user
     */
    public static function createPlan($donationAmount)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
            $name = $donationAmount->badge->name ?? $donationAmount->amount."_plan";
            $plan = $stripe->plans->create([
                'amount' => $donationAmount->amount,
                'currency' => config('common.stripe_currency'),
                'interval' => 'month',
                'product' => ['name' => $name],
            ]);
            $donationAmount->update(['stripe_plan_id' => $plan->id]);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $customer;
    }

    /**
     *  create a stripe customer for a user
     */
    public static function addCustomer($user)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));

            $customer = $stripe->customers->create([
                'email' => $user->email,
                'name' => $user->name,
                'id' => $user->id,
                // 'default_source' => $cardTokens,
            ]);

            $user->update(['stripe_customer_id' => $customer->id]);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $customer;
    }

    public static function updateCustomer($user)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));

            $customer = $stripe->customers->update($user->stripe_customer_id, [
                'email' => $user->email,
                'name' => $user->name,
                'phone' => $user->phone_number,
            ]);

            $user->update(['stripe_customer_id' => $customer->id]);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $customer;
    }

    public static function getCustomer($user)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
            $customer = $stripe->customers->retrieve($user->stripe_customer_id);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $customer;
    }

    public static function createCard($user, $data)
    {
        try {
            if (!$user->stripe_customer_id) {
                self::addCustomer($user);
            }
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
            $card = $stripe->customers->createSource($user->stripe_customer_id, [
                'source' => $data
            ]);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $card;
    }

    public static function allCards($user)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
            $cards = $stripe->customers->allSources($user->stripe_customer_id, ['object' => 'card', 'limit' => 3]);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $cards->data;
    }

    public static function deleteCard($user, $cardId)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
            $cards = $stripe->customers->deleteSource($user->stripe_customer_id, $cardId, []);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $cards->data;
    }

    public static function createDirectCharge($user, $cardToken, $amount, $desc=null)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));

            $charge = $stripe->charges->create([
                'amount'        => $amount,
                'currency'      => config('common.stripe_currency'),
                'source'        => $cardToken,
                'description'   => $desc ?? 'Charges for Donation',
            ]);

        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }

        return $charge;
    }

    public static function createCharge($user, $cardId, $amount, $desc=null)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
            $customer = $stripe->customers->retrieve($user->stripe_customer_id);

            $charge = $stripe->charges->create([
                'amount'        => $amount,
                'currency'      => config('common.stripe_currency'),
                'customer'      => $customer,
                'source'        => $cardId,
                'description'   => $desc ?? 'Charges for Donation',
            ]);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }

        return $charge;
    }

    public static function createRefund($user, $order)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
            $refund = $stripe->refunds->create([
                'charge' => $order->charge_id,
                'reason' => "requested_by_customer",
            ]);
            $order->update(['refund_id' => $refund->id]);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $refund;
    }

    public static function createPrice($priceAmount)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));

            $price = $stripe->prices->create([
                'unit_amount' => $priceAmount->amount * 100,
                'currency' => config('common.stripe_currency'),
                'recurring' => ['interval' => 'month'],
                'product_data' => ['name' => $priceAmount->name],
            ]);

            $priceAmount->update(['stripe_price_id' => $price->id]);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }

        return $price;
    }

    public static function cancelSubscription($user)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
            if ($user->stripe_subscription_id) {
                $cancel = $stripe->subscriptions->cancel($user->stripe_subscription_id, []);
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $cancel;
    }

    public static function createSubscription($user, $stripePrice)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
            $priceId = $stripePrice->stripe_price_id;
            $subscription = null;

            if (!$priceId) {
                $price = self::createPrice($stripePrice);
                if ($price) {
                    $priceId = $price->id;
                }
            }

            if (!$user->stripe_customer_id) {
                $customer = self::addCustomer($user);
            }

            if ($priceId) {
                if ($user->stripe_subscription_id) {
                    $subscription = $stripe->subscriptions->update($user->stripe_subscription_id, [
                        'items' => [
                            ['price' => $priceId],
                        ],
                    ]);
                } else {
                    $subscription = $stripe->subscriptions->create([
                        'customer' => $user->stripe_customer_id,
                        'items' => [
                            ['price' => $priceId],
                        ],
                    ]);
                    $user->update(['stripe_subscription_id' => $subscription->id]);
                }
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $subscription;
    }

    public static function createAccount($user)
    {
        $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
        $organisation = $user->organisation;

        $account = $stripe->accounts->create([
            'type' => 'custom',
            'country' => config('common.country_short_code'),
            'email' => $user->email,
            'capabilities' => [
                'card_payments' => ['requested' => true],
                'transfers' => ['requested' => true],
            ],
            'business_type' => 'individual',
            'individual' => [
                'address' => [
                    'line1' => '93 Creegans Road',
                    'city' => 'City',
                    'country' => config('common.country_short_code'),
                    'state' => 'NSW',
                    'postal_code' => '2474',
                ],
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'gender' => 'male',
                'phone' => $user->phone_number,
                'dob' => ['day' => rand(0, 31), 'month' => rand(1, 12), 'year' => 1990],
            ],
            'business_profile' => [
                'mcc' => '5734',
                'name' => 'sfd',
                'support_phone' => $user->phone_number,
                'support_email' => $user->email,
                'support_address' => [
                    'line1' => '93 Creegans Road',
                    'city' => 'city',
                    'country' => config('common.country_short_code'),
                    'state' =>'NSW',
                    'postal_code' => '2474',
                ],
                'support_url' => 'https://dev.webdevelopmentsolution.net/mps-pool/',
                'url' => 'https://dev.webdevelopmentsolution.net/mps-pool/',
            ],
            'tos_acceptance' => [
                'date' => time(),
                'ip' => request()->ip(),
            ],
            'company' => [
                'name' => 'business',
                'phone' => '6543211512',
                'tax_id' => '65424',
                'address' => [
                    'line1' => 'address',
                    'city' => 'address',
                    'country' => config('common.country_short_code'),
                    'postal_code' => '321654',
                ],
                'directors_provided' => true,
                'owners_provided' => true,
                'executives_provided' => true,
            ],
        ]);

        $user->update(['stripe_account_id' => $account->id]);
    }

    public static function updateAccount($user)
    {
        $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
        $organisation = $user->organisation;

        $account = $stripe->accounts->update($user->stripe_customer_id, [
            'business_type' => 'individual',
            'individual' => [
                'address' => [
                    'line1' => $organisation->business_address ?? '93 Creegans Road',
                    'city' => $organisation->business_address,
                    'country' => config('common.country_short_code'),
                    'state' => $organisation->business_state ?? 'NSW',
                    'postal_code' => $organisation->business_zip_code ?? '2474',
                ],
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'gender' => 'male',
                'phone' => $user->phone_number,
                'dob' => ['day' => rand(0, 31), 'month' => rand(1, 12), 'year' => 1990],
            ],
            'business_profile' => [
                'mcc' => '5734',
                'name' => $organisation->business_name,
                'support_phone' => $user->phone_number,
                'support_email' => $user->email,
                'support_address' => [
                    'line1' => $organisation->business_address ?? '93 Creegans Road',
                    'city' => $organisation->business_address,
                    'country' => config('common.country_short_code'),
                    'state' => $organisation->business_state ?? 'NSW',
                    'postal_code' => $organisation->business_zip_code ?? '2474',
                ],
                'support_url' => 'https://dev.webdevelopmentsolution.net/mps-pool/',
                'url' => 'https://dev.webdevelopmentsolution.net/mps-pool/',
            ],
            'tos_acceptance' => [
                'date' => time(),
                'ip' => request()->ip(),
            ],
        ]);
    }

    public static function addBankAccount($user, $organisationBankDetail)
    {
        $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
        // Testing // BSB  110000 // ACC    000123456 // admin- acct_1H80QXLMjxxHoNqS
        try {
            if (!$user->stripe_customer_id) {
                self::createAccount($user);
            } else {
                self::updateAccount($user);
            }

            if ($organisationBankDetail && !$organisationBankDetail->stripe_bank_account_id) {
                $account = $stripe->accounts->createExternalAccount($user->stripe_customer_id,
                    ['external_account' => [
                        'object' => 'bank_account',
                        'country' => config('common.country_short_code'),
                        'currency' => config('common.stripe_currency'),
                        'account_holder_name' => $organisationBankDetail->account_name,
                        'account_number' => $organisationBankDetail->account_number,
                        'routing_number' => $organisationBankDetail->bsb_number,
                    ]]
                );

                $organisationBankDetail->update(['stripe_bank_account_id' => $account->id]);
                return $account;
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }

        return false;
    }

    public static function transferToBankAccount($user, $inspection)
    {
        $transfer = false;

        try {
            $stripe = new \Stripe\StripeClient(config('common.stripe_secret_key'));
            $bankDetail = $user->organisationBankDetail;
            $order = $inspection->order;
            $comissionn = ($order->amount * 20) / 100;
            $amount = ($order->amount - $comissionn) * 100;

            if ($user->stripe_customer_id) {
                $transfer = $stripe->transfers->create([
                    'amount' => $amount,
                    'currency' => config('common.stripe_currency'),
                    'destination' => $user->stripe_customer_id,
                ]);
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return false;
        }
        return $transfer;
    }
}
