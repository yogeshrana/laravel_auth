<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;
class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $appends = [
        'profile_photo_url',
        'user_name'
    ];

    public function getProfilePhotoUrlAttribute()
    {
        if (preg_match('(https://|http://)', $this->profile_image) === 1) {
            return $this->profile_image;
        }
        return !empty($this->profile_image) ? asset("storage/assets/images/$this->profile_image") : asset('storage/assets/images/user.png');
    }
    public function getUserNameAttribute() //mutators
    {
        return Auth::user()->name;   
    }

    public function scopeUserss() //scope
    {
        return "asdf";
    }

    public function isAdmin() //check
    {
        return $this->id == 1;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
