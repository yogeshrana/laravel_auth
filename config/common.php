<?php
return [
    // General
   // 'email'                 => env('MAIL_USERNAME', ''),
    'stripe_secret_key'     => env('STRIPE_SECRET_KEY'),
    'stripe_publish_key'    => env('STRIPE_PUBLISH_KEY'),
    'stripe_currency'       => env('STRIPE_CURRENCY'),
    // Reasons for order reports
    // 'report_reasons'        => [
    //     'Missing Item',
    //     'Wrong Item',
    //     'Damaged Item',
    // ]
];